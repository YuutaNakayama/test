#ifndef DEFINES
#define DEFINES

#define ONE_BLOCK 				182
#define HALF_BLOCK				91
#define CENTER_BLOCK 			45
#define BLOCK 37
#define TREAD_WIDTH       90.3
#define QUARTER_TURN (TREAD_WIDTH * 3.141592) / 4.0

#define TEST_SPEED				100
#define TURN_SPEED				100
#define SEARCH_SPEED 			300

#define TH_SIDEWALL 			200
#define TH_ONE_SIDEWALL		100
#define TH_FRONTWALL			140

#define MODE_RUNNING 			(0)
#define MODE_SKIP_STOP 		(0)
#define MODE_STOP    			(1)
#define MODE_SKIP_START 	(2)

#define KP 2.0

#define KI 0.01

/*enum Direction{
	LEFT,
	RIGHT,
	FRONT,
	LEFT_TURN,
	RIGHT_TURN,
	TURN
};*/

inline int to_mm(int _pulse){
	return 2000.0/410.0 * _pulse;
	//return 2000.0/500.0 * _pulse;
}

inline int to_pulse(double _mm){
	return (double)(1000.0/410.0 * _mm);
	//return (double)(1000.0/420 * _mm);
}

#endif
//1000パルスで407.5mm
