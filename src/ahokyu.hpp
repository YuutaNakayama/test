#ifndef   AHOKYU
#define   AHOKYU

#include <mbed.h>
#include "defines.hpp"
#include "mslm/mslm.h"
#include "machine.hpp"


//kyuusin
void ahokyu(Machine &machine, MapMbed &map, BusOut &led);


#endif
