#include <mbed.h>
#include "mslm/mslm.h"
#include "machine.hpp"
#include "defines.hpp"
#include "search_kyushin.hpp"
#include "ahokyu.hpp"

AnalogOut ref(p18);
Serial pc(USBTX,USBRX);

Switch sw_r(p5,PullUp);//right
Switch sw_l(p6,PullUp);//left
Switch sw_d(p7,PullUp);//decide
Switch sw_c(p8,PullUp);//cancel

//DistanceSensor rs(p15), ls(p16), fs(p17);

DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);
BusOut led(LED1, LED2, LED3, LED4);

Machine machine;
MapMbed map;

namespace Main{
  enum Mode{
    SelectMode,     //0
    KyushinMode,    //1
    SerialMode,     //2
    MoveMode,       //3
    CenterMode,//4
    HalfMode,//5
    CenterHalf,//6
    TestTurnMode,   //4
    CheckWallMode,   //5
    ahokyu
  };
}

int main() {
  pc.baud(115200);

  int mode = Main::SelectMode;
  int cnt = 0;


  while(1){
    switch (mode) {
      case Main::SelectMode:
      if(sw_r.update()){
        cnt++;
      }else if(sw_l.update()){
        cnt--;
      }else if(sw_d.update()){
        mode = cnt;
      }
      led = cnt;
      break;

      case Main::KyushinMode:
      led = 0b1001;
      wait(0.5);
      map.set_start(0,0);
      map.set_goal(3,8);
      // map.set_goal(3,8);
      map.set_size(16,16);
      map.init();
      machine.motor_on();
      wait(0.3);
      search_kyushin(machine, map, led);

      // map.set_goal(0,0);
      // map.init();
      // machine.motor_on();
      // search_adachi(machine, map, _led);

      wait(1);
      map.writefile_mapdata();
      wait(1);
      led = 0b1111;
      wait(0.5);
      led = 0b0000;

      // map.set_start(3, 3);
      // map.set_goal(0, 0);
      // machine.motor_on();
      // search_adachi(machine, map, _led);

      mode = Main::SelectMode;

      break;

      case Main::SerialMode:
      pc.printf("\r\b\r");
      pc.printf("LS = %4d || FS = %4d || RS = %4d\r\n ", machine.ls.get_val(), machine.fs.get_val(), machine.rs.get_val());

      cnt++;
      if(sw_c.update()){
        mode = Main::SelectMode;
        break;

        case Main::MoveMode:
        led = 0b1111;
        wait(0.5);
        led = 0b0000;
        machine.motor_on();
        //machine.move(HALF_BLOCK, TEST_SPEED);
        //machine.motor_stop();
        /*wait(0.5);
        machine.turn(LEFT_MACHINE);
        machine.motor_stop();
        wait(0.5);
        machine.turn(LEFT_MACHINE);
        machine.motor_stop();
        wait(0.5);
        machine.turn(LEFT_MACHINE);
        machine.motor_stop();
        wait(0.5);
        machine.turn(LEFT_MACHINE);
        machine.motor_stop();
        wait(0.5);*/

        // machine.move_p(HALF_BLOCK, SEARCH_SPEED);
        //machine.move_test_pulse(1000);
        machine.move_test_mm(ONE_BLOCK*3);

        //machine.rotation(2000);
        // machine.move_p(ONE_BLOCK*8, TEST_SPEED);
        machine.motor_stop();
        wait(0.5);
        machine.motor_off();
        mode = Main::SelectMode;
        break;

        case Main::CenterMode://4
        led = 0b1111;
        wait(0.5);
        led = 0b0000;
        machine.motor_on();
        machine.move(CENTER_BLOCK, TEST_SPEED);
        machine.motor_stop();
        wait(0.5);
        machine.motor_stop();
        wait(0.5);
        machine.motor_off();
        mode = Main::SelectMode;
        break;


        case Main::HalfMode://5
        led = 0b1111;
        wait(0.5);
        led = 0b0000;
        machine.motor_on();
        machine.move(HALF_BLOCK, TEST_SPEED);
        machine.motor_stop();
        wait(0.5);
        machine.motor_stop();
        wait(0.5);
        machine.motor_off();
        mode = Main::SelectMode;
        break;


        case Main::CenterHalf://6
        led = 0b1111;
        wait(0.5);
        led = 0b0000;
        machine.motor_on();
        machine.move(HALF_BLOCK+CENTER_BLOCK, TEST_SPEED);
        machine.motor_stop();
        wait(0.5);
        machine.motor_stop();
        wait(0.5);
        machine.motor_off();
        mode = Main::SelectMode;
        break;

        case Main::TestTurnMode:
        led = 0b0110;
        wait(0.5);
        machine.motor_on();
        machine.turn(LEFT_MACHINE);
        wait(0.3);
        machine.motor_stop();
        wait(0.5);
        machine.motor_off();
        wait(0.5);
        machine.motor_on();
        machine.turn(LEFT_MACHINE);
        wait(0.3);
        machine.motor_stop();
        wait(0.5);
        machine.motor_off();
        wait(0.5);
        machine.motor_on();
        machine.turn(LEFT_MACHINE);
        wait(0.3);
        machine.motor_stop();
        wait(0.5);
        machine.motor_off();
        wait(0.5);
        machine.motor_on();
        machine.turn(LEFT_MACHINE);
        wait(0.3);
        machine.motor_stop();
        wait(0.5);
        machine.motor_off();
        mode = Main::SelectMode;
        break;

        case Main::CheckWallMode:
        led = 0b0000;
        led1 = machine.is_opened_wall(LEFT_MACHINE);
        led2 = machine.is_opened_wall(FRONT_MACHINE);
        led3 = machine.is_opened_wall(FRONT_MACHINE);
        led4 = machine.is_opened_wall(RIGHT_MACHINE);
        if(sw_c.update()){
          mode = Main::SelectMode;
          led = 0b1111;
          wait(0.3);
        }
        break;

        case Main::ahokyu:
        led = 0b1001;
        wait(0.5);
        map.set_start(0,0);
        map.set_goal(3,8);
        // map.set_goal(3,8);
        map.set_size(16,16);
        map.init();
        machine.motor_on();
        wait(0.3);
        ahokyu(machine, map, led);

        // map.set_goal(0,0);
        // map.init();
        // machine.motor_on();
        // search_adachi(machine, map, _led);

        wait(1);
        map.writefile_mapdata();
        wait(1);
        led = 0b1111;
        wait(0.5);
        led = 0b0000;

        // map.set_start(3, 3);
        // map.set_goal(0, 0);
        // machine.motor_on();
        // search_adachi(machine, map, _led);

        mode = Main::SelectMode;

        break;

      }
    }
  }


  /*if(sw_r.update()){
  led1 = 1;
  wait(0.5);
  led1 =0;
}
if(sw_l.update()){
led2 = 1;
wait(0.5);
led2 =0;
}
if(sw_d.update()){
led3 = 1;
wait(0.5);
led3 =0;
}
if(sw_c.update()){
led4 = 1;
wait(0.5);
led4 =0;
}
}


// led = 0b0100;
//   _ref = 0.03/3.3;
//   _m3_l = 1;
//   _clock_l = 0;
//   _cwccw_l = 1;
pc.printf("\r\b\r");
//   _reset_l = 0;
//   _m3_r = 1;
//   _clock_r = 0;
//   _cwccw_r = 1;
//   _reset_r = 0;
//
//   led = 0b1000;
//   wait(0.5);
//   led = 0b0100;
//   wait(0.5);
//   led = 0b0010;
//   wait(0.5);
//   led = 0b0001;
//   wait(0.5);
//
//   _m3_l = 0;
//   _m3_r = 0;
//
//   led = 0b0110;
//   wait(1);
//
//   while(1) {
//     _clock_l = 1;
//     _clock_r = 1;
//     wait(0.01);
//       _clock_l = 0;
//       _clock_r = 0;
//       wait(0.01);
//   }*/
}
