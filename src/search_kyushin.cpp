#include "search_kyushin.hpp"

enum Pattern{
	INIT,
	CHECK,
	GO_STRAIGHT,
	GO_LEFT,
	GO_RIGHT,
	BACK_TURN,
	GOAL,
	WRITE_MAP,
};

void search_kyushin(Machine &machine, MapMbed &map, BusOut &led){
	int mode = INIT;
	uint8_t walls = 0;

	while(1){
		switch(mode){
			case INIT:
			machine.turn(LEFT_MACHINE);
			wait(0.3);
			machine.motor_stop();
			wait(0.5);
			machine.back();
			machine.move_test_mm(BLOCK);
			machine.motor_stop();
			wait(0.5);
			machine.turn(RIGHT_MACHINE);
			machine.motor_stop();
			wait(0.5);
			machine.back();
			//machine.move_test_mm(BLOCK);
			machine.move_p(BLOCK + HALF_BLOCK, TEST_SPEED);
			// machine.move(DEFAULT_SPEED, CENTER_BLOCK);
			//machine.motor_stop();
			//wait(0.5);
			//machine.move_d(SEARCH_SPEED, HALF_BLOCK, MODE_SKIP_STOP);
			//machine.move_p(HALF_BLOCK, SEARCH_SPEED);  ↑に統合
			//machine.motor_stop();
			//wait(5);
			mode = CHECK;
			break;

			case CHECK:
			map.update(!machine.is_opened_wall(LEFT_MACHINE),
			!machine.is_opened_wall(FRONT_MACHINE),
			!machine.is_opened_wall(RIGHT_MACHINE));
			map.make_walkmap();

			if(map.check_goal()){mode = GOAL;break;}
			if(map.get_cnt_walkmap()==255){mode = WRITE_MAP;break;}

			if(machine.is_opened_wall(FRONT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(FRONT_MACHINE)==1 )mode = GO_STRAIGHT;
			else if(machine.is_opened_wall(LEFT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(LEFT_MACHINE)==1 )mode = GO_LEFT;
			else if(machine.is_opened_wall(RIGHT_MACHINE) && map.get_cnt_walkmap()-map.get_cnt_walkmap(RIGHT_MACHINE)==1 )mode = GO_RIGHT;
			else mode = BACK_TURN;

			break;

			case GO_STRAIGHT:
			// machine.motor_stop();
			// wait(0.2);
			//map.writefile_mapdata();
			map.change_direction(FRONT_MACHINE);
			machine.move_p(ONE_BLOCK, SEARCH_SPEED);
			mode = CHECK;
			break;


			case GO_LEFT:
			// machine.motor_stop();
			// wait(0.2);
			map.change_direction(LEFT_MACHINE);
			//machine.move_d(SEARCH_SPEED, ONE_BLOCK/2, MODE_SKIP_START);
			machine.move_d(ONE_BLOCK/2, MODE_SKIP_START);
			machine.motor_stop();
			led = 0b0001;
			wait(0.2);

			machine.turn(LEFT_MACHINE);
			machine.motor_stop();
			//machine.move_d(SEARCH_SPEED, ONE_BLOCK/2, MODE_SKIP_STOP);
			machine.move_d(ONE_BLOCK/2, MODE_SKIP_STOP);
			mode = CHECK;
			break;


			case GO_RIGHT:
			map.change_direction(RIGHT_MACHINE);

			//machine.move_d(SEARCH_SPEED, ONE_BLOCK/2, MODE_SKIP_START);
			machine.move_d(ONE_BLOCK/2, MODE_SKIP_START);
			machine.motor_stop();
			led = 0b1000;
			wait(0.2);

			machine.turn(RIGHT_MACHINE);
			machine.motor_stop();
			//machine.move_d(SEARCH_SPEED, ONE_BLOCK/2, MODE_SKIP_STOP);
			machine.move_d(ONE_BLOCK/2, MODE_SKIP_STOP);
			mode = CHECK;
			break;


			case BACK_TURN:
			//machine.move_d(SEARCH_SPEED, ONE_BLOCK/2, MODE_SKIP_START);
			map.change_direction(TURN_MACHINE);
			machine.move_d(ONE_BLOCK/2, MODE_SKIP_START);
			machine.motor_stop();
			wait(0.5);

			machine.turn(RIGHT_MACHINE);
			wait(0.3);
			machine.motor_stop();
			wait(0.5);
			machine.back();
			machine.move_test_mm(BLOCK);
			machine.motor_stop();
			wait(0.5);
			machine.turn(RIGHT_MACHINE);
			machine.motor_stop();
			wait(0.5);
			machine.back();
			//machine.move_test_mm(BLOCK);
			//machine.move_p(/*CENTER_*/BLOCK, TEST_SPEED);
			// machine.move(DEFAULT_SPEED, CENTER_BLOCK);
			machine.motor_stop();
			wait(0.5);
			//machine.move_d(SEARCH_SPEED, ONE_BLOCK/2, MODE_SKIP_STOP);
			machine.move_d(ONE_BLOCK/2 + BLOCK, MODE_SKIP_STOP);
			mode = CHECK;
			break;

			case GOAL:
			//machine.move_d(SEARCH_SPEED, ONE_BLOCK/2, MODE_SKIP_START);
			machine.move_p(ONE_BLOCK/2, SEARCH_SPEED);
			machine.motor_stop();
			wait(0.2);
			machine.turn(TURN_MACHINE);
			machine.motor_stop();
			wait(0.2);
			machine.motor_off();
			return;
			// mode = WRITE_MAP;
			// break;

			case WRITE_MAP:
			wait(1);
			map.writefile_mapdata();
			wait(1);
			led = 0b11111;
			wait(0.5);
			led = 0b0000;
			return;
		}
	}
}
