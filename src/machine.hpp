#ifndef MACHINE_H
#define MACHINE_H

#include <mbed.h>
#include "defines.hpp"
#include "mslm/mslm.h"

class Machine{
private:
  StepMotor lm, rm;

  AnalogOut lef;

public:

  DistanceSensor ls, fs, rs;
  Machine():
  lm(p11, p10, p12, true, p9),rm(p23, p24, p25, false, p26),
  ls(p16), fs(p17), rs(p15),
  lef(p18)
  {
    lef = 0.08/3.3;
  }

  void motor_on(){
    lm.Start();
    rm.Start();
    wait(0.5);
  }

  void motor_off(){
    lm.kill();
    rm.kill();
  }

  void motor_stop(){
    lm.update(0);
    rm.update(0);
  }

  void move_test_pulse(int _pulse){
    rm.reset_cnt();
    lm.reset_cnt();
    while(rm.pulse_cnt() <= _pulse){
      lm.update(TEST_SPEED);
      rm.update(TEST_SPEED);
    }
    rm.reset_cnt();
    lm.reset_cnt();
  }

  void move_test_mm(int _mm){
    rm.reset_cnt();
    lm.reset_cnt();
    while(rm.pulse_cnt() <= to_pulse(_mm)){
      lm.update(TEST_SPEED);
      rm.update(TEST_SPEED);
    }
    rm.reset_cnt();
    lm.reset_cnt();
  }

  void rotation(int _mm){
    rm.reset_cnt();
    lm.reset_cnt();
    int aa = 200;
    while(rm.pulse_cnt() <= to_pulse(_mm)){
      lm.update(aa);
      rm.update((-1)*aa);
      wait(1);
      aa+=50;
    }
    rm.reset_cnt();
    lm.reset_cnt();
  }

  void turn(int _dir){
			int lm_speed = (_dir==LEFT_MACHINE)?-TURN_SPEED:TURN_SPEED;
			int rm_speed = (_dir==LEFT_MACHINE)?TURN_SPEED:-TURN_SPEED;
			int pulse = (_dir==TURN_MACHINE)?to_pulse(QUARTER_TURN)*2:to_pulse(QUARTER_TURN);
			lm.reset_cnt();
			rm.reset_cnt();

			while(1){
				if(lm.pulse_cnt() > pulse || rm.pulse_cnt() > pulse)
				break;
				lm.update(lm_speed);
				rm.update(rm_speed);
			}
      motor_stop();
			wait(0.2);
			lm.reset_cnt();
			rm.reset_cnt();
		}



  bool is_opened_wall(int _direction){
    if(LEFT_MACHINE == _direction && TH_ONE_SIDEWALL + 25 < ls.get_val())return true;
    else if(RIGHT_MACHINE == _direction && TH_ONE_SIDEWALL + 20 < rs.get_val())return true;
    else if(FRONT_MACHINE == _direction && TH_FRONTWALL  + 30 < fs.get_val())return true;
    else return false;
  }


  #define ACCEL_P 0.8

  void move_d(int distance,int _mode){
    int speed = 500;
    lm.reset_cnt();
    rm.reset_cnt();
    //pluse_diff = 0;
    int seq = (_mode==MODE_SKIP_START)?1:0;
    int motor_pulse = (rm.pulse_cnt() + lm.pulse_cnt())/2;
    int count = to_pulse(distance);
    const int lowest_speed = 150;
    int now_speed = lowest_speed; //最低スピード代入
    int acc_count = (speed - now_speed) / ACCEL_P;  //加速に必要なパルス数
    int keep_count = count - (acc_count * 2);
    int keep_speed = 0;

    if ((acc_count*2) > count) {
      keep_count = 0;
      acc_count -= ((acc_count * 2) - count)/2;
    }
    //now_speed = speed_ave;
    while(motor_pulse  <= count)
    {
      motor_pulse = (rm.pulse_cnt() + lm.pulse_cnt())/2;
      if(motor_pulse % 2 == 0)
      {
        switch(seq)
        {
          case 0:
          now_speed = lowest_speed + (motor_pulse * ACCEL_P);
          if(motor_pulse > acc_count){ seq = 1;  break; }
          break;
          case 1:
          // Adjust deceleration distance by sensor value
          if(motor_pulse > acc_count + keep_count && _mode!=MODE_RUNNING){seq = 2; break; }
          now_speed = speed;
          break;
          case 2:
          now_speed = lowest_speed +(count - motor_pulse)*ACCEL_P;
          if(now_speed < lowest_speed) now_speed = lowest_speed;
          break;
        }
      }

      if(keep_speed != now_speed){
        keep_speed = now_speed;
        printf("%d\n\r",now_speed);
      }
      // if((check_front_left()<=FRONTWALL-10 || check_front_right()<=FRONTWALL-10) &&
      //    ( motor_pulse >= HALFBLOCK_PULSE ))break;
      p_control(now_speed); //P制御ありラン
    }
    if(_mode==MODE_STOP)motor_stop();
    lm.reset_cnt();
    rm.reset_cnt();
  }


  inline void p_control(int speed){
    int LS = ls.get_val();
    int RS = rs.get_val();
    int FS = fs.get_val();
    const int sensor_sum = LS + RS;


    if(sensor_sum > TH_SIDEWALL){
      if(LS > TH_ONE_SIDEWALL && RS < TH_ONE_SIDEWALL){
        LS = TH_SIDEWALL - RS;
      }
      else if(RS > TH_ONE_SIDEWALL && LS < TH_ONE_SIDEWALL){
        RS = TH_SIDEWALL - LS;
      }else{
        LS=RS=0;
      }
    }

     if(FS < TH_FRONTWALL){
       LS = RS = 0;
           lm.update(0);
           rm.update(0);

     }

    int sensor_diff = LS - RS;
    //lm.update(speed - (sensor_diff * KP));
    //rm.update(speed + (sensor_diff * KP));
    //const int sensor_diff = (LS - 110) - (RS - 125);

    int l_speed = speed - (sensor_diff * KP);
    int r_speed = speed + (sensor_diff * KP);

    lm.update(l_speed);
    rm.update(r_speed);
    wait(0.01);
    //printf("%d %d\n\r", l_speed, r_speed);
  }


  void move_p(int _mm, int _speed/*, bool _stop_enable*/){
    lm.reset_cnt();
    rm.reset_cnt();
    while(rm.pulse_cnt() <= to_pulse(_mm)){
      p_control(_speed);
    }
    /*if(_stop_enable){
    lm.update(0);
    rm.update(0);
  }*/
  lm.reset_cnt();
  rm.reset_cnt();
}

void move(int _mm, int _speed/*, bool _stop_enable*/){
  lm.reset_cnt();
  rm.reset_cnt();
  while(lm.pulse_cnt() <= to_pulse(_mm)){
    p_control(_speed);
  }
  /*if(_stop_enable){
  lm.update(0);
  rm.update(0);
}*/
lm.reset_cnt();
rm.reset_cnt();
}

void back(){
  lm.update(-TEST_SPEED);
  rm.update(-TEST_SPEED);
  lm.reset_cnt();
  rm.reset_cnt();
  wait(1);
  motor_stop();
  wait(0.5);
}
};

#endif

// DigitalOut m3_l(p9);
// DigitalOut reset_l(p10);
// DigitalOut clock_l(p11);
// DigitalOut cwccw_l(p12);
//
// DigitalOut clock_r(p23);
// DigitalOut reset_r(p24);
// DigitalOut cwccw_r(p25);
// DigitalOut m3_r(p26);
