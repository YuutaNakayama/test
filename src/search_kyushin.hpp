#ifndef   SEARCH_KYUSHIN
#define   SEARCH_KYUSHIN

#include <mbed.h>
#include "defines.hpp"
#include "mslm/mslm.h"
#include "machine.hpp"


//kyuusin
void search_kyushin(Machine &machine, MapMbed &map, BusOut &led);


#endif
